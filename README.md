# Atlas Fantasia Map Editor

[![License: LGPL v3](https://img.shields.io/badge/License-LGPLv3-blue.svg)](https://www.gnu.org/licenses/lgpl-3.0.html)
[![npm version](http://img.shields.io/npm/v/@atlas-fantasia/map.svg)](https://npmjs.org/package/@atlas-fantasia/map "View this project on npm")
[![Bundle size](https://img.shields.io/bundlephobia/minzip/@atlas-fantasia/map)](https://bundlephobia.com/result?p=@atlas-fantasia/map)
[![Website Monitor](https://img.shields.io/website?url=https%3A%2F%2Fwww.atlasfantasia.com%2F)](https://www.atlasfantasia.com/)

![Example map](map.jpg "Atlas Fantasia Map")

The Atlas Fantasia map is a JavaScript library for drawing and rendering fantasy maps in a web browser. It intentionally does _not_ come with a UI framework for editing (beyond basic navigation tools such as zooming and panning), but does supply a powerful and rich API with which your UI can interact and edit the maps.

The library consists of a JavaScript file and a CSS file. No other dependencies are required. All rendering and texture generation are done within the browser; no external assets will be downloaded.

This project had previously reached a workable stage, but limitations in the technologies chosen necessitated a rewrite. The previous iteration of the map may be used at [https://www.atlasfantasia.com/](https://www.atlasfantasia.com/).

The Atlas Fantasia map editor is licenced under the LGPLv3. Please see the supplied [license](LICENCE).

## Installation for Use in Projects

If you are using Webpack or some other compatible system to build your project, you can install the library with:

```bash
npm install @atlas-fantasia/map
```

Otherwise you can download the compiled script from the `dist` directory and embed it in your HTML with

```xml
<script src="/..path../atlas-fantasia.js"></script>
```

Ensure that the supplied styles in `/dist/style.css` are included on your page.

## Installation for Development

This project is set up to be built with Node/NPM tools. It uses Gulp and Webpack.

To install the build tools, navigate to the project directory and run:

```bash
npm install
```

Once the packages have downloaded you can run the build system. To build once, run:

```bash
gulp build
```

To watch for, and build on changes to `js` or `scss` files, run

```bash
gulp watch
```

Compiled JavaScript and CSS files will be placed in the `dist` directory.

## Initialisation in a Project

In your JavaScript project you may access `AtlasFantasiaMap` through the `window` object (can usually be inferred). Instantiate a new map with `var map = new AtlasFantasiaMap`.

If using a build system like Webpack, the library can be imported and instantiated with

```js
import Map from '@atlas-fantasia/map'
...
let map = new Map.map
```

Note that the `return` value must be stored to a variable, e.g. `map`. This variable will be used to control the map instance. For example, to initialise the map on an existing HTML element with ID `#map`, use `map.init( '#map' )`.

The API with which the map can be edited or rendered is detailed below.

## API Overview

### `map.init( selector[, config ] )`

Initialises the map.

`selector` may be a string containing a CSS selector, such as `'#map'` or `'.container > div'`, or a direct reference to a HTML element. If the element does not have a height it will be given a default height with a ratio 0.58 of its width.

`config` is an optional argument consisting of an object literal. It defines configuration options for the map, with the option types as the object keys and their corresponding values as the value pairs. The following options are available:

-   `data`: This allows you to load a previously created map in the view. Value is an object containing exported data from a previous map edit.
-   `fonts`: This allows you to define fonts used in the map. Without this text may not be displayed correctly within the map. Fonts are downloaded on initialisation; this may make initialisation take several seconds depending on connection speed. Value is an array of objects, of the form: `{ name: 'font-name', path: 'font-path' }`.
-   `startTerrain`: This allows you to set the default terrain with which the map is drawn if there is none already set in `data`. Value is a string that is either `blank`, `ocean`, `plains`, `desert`, `water`, `snow`, or `stone`.

### `map.destroy()`

Removes the map and cleans up memory and event listeners.

### `map.get( key[, argument ] )`

Gets a parameter value from the map. All values returned are new variables. Altering a returned value will not alter the state of the map.

`key` must be a string corresponding to the parameter name. Some parameters require an additional argument; this may be supplied as the `argument`. The following parameters are supported:

-   `action`: Returns a string containing the action currently being undertaken by the map editor. See the section under `map.action(...)` for a list of possible actions. Note that many actions called with `map.action(...)` are instantaneous, and the value returned by this parameter is the one _currently_ being done. For example, running the action `terrain.fill` is instantaneous so getting the action after it's run will return a result of `none` rather than `terrain.fill`. The action `terrain.paint` requires ongoing user input, so getting the action will return `terrain.paint` as long as it is being executed.
-   `color`: Alias for `color.terrain`.

-   `color.terrain`: Returns a string containing the CSS colour code used by that colour parameter in the map terrain. Requires an argument that is a string of either `blank`, `ocean`, `plains`, `desert`, `water`, `snow`, or `stone`.

-   `color.texture`: Returns a string containing the CSS colour code used by that colour parameter in the map textures. Requires an argument that is a string of either `waves`, `swamp`, or `hatching`.
-   `colors`: Alias for `colors.terrain`.

-   `colors.terrain`: Returns an object literal containing the keys `blank`, `ocean`, `plains`, `desert`, `water`, `snow`, and `stone`, with the corresponding CSS colour codes currently set for each key, as used in the map for terrain colour parameters.

-   `colors.textures`: Returns an object literal containing the keys `waves`, `swamp`, and `hatching`, with the corresponding CSS colour codes currently set for each key, as used in the map for texture colour parameters.

-   `data`: Returns an object literal containing all data from which the currently drawn map can be re-rendered. Useful for saving the map to load again in the future.

-   `zoom`: Returns an integer between 0 and 4 representing the current zoom level of the map.

### `map.set( key, argument )`

Sets a parameter value for the map.

`key` must be a string corresponding to the parameter name. `argument` is the value to which the parameter will be set. The following parameters are supported:

-   `colors`: Alias for `colors.terrain`.

-   `colors.terrain`: Sets the colours used when rendering the map terrain; for example setting the colour of the ocean. Requires an argument that is an object literal containing the keys `blank`, `ocean`, `plains`, `desert`, `water`, `snow`, and/or `stone`, with corresponding CSS colour codes for each key. Returns the same object on a success. Default values are `{ blank: '#000000', ocean: '#416091', grassland: '#268221', desert: '#efdd77', water: '#4996b2', snow: '#dae4e8', stone: '#484d57' }`.

-   `colors.textures`: Sets the colours used when rendering the map texture layer; for example setting the colour of the swamp texture. Requires an argument that is an object literal containing the keys `waves`, `swamp`, and/or `hatching`, with corresponding CSS colour codes for each key. Returns the same object on a success. Default values are `{ waves: '#273c59', swamp: '#293d28', hatching: '#000000' }`.

-   `terrain.outline`: Sets the thickness of the outline drawn around blank and water/ocean terrain. Requires an argument that is a number. Maximum is 5, minimum is 0, and any decimals will be truncated. A value of 0 will cause no outlines to be drawn. Default value is `2`.

-   `terrain.blur`: Sets the amount that different terrain types are mixed at their edges. Requires an argument that is a number. Maximum is 5, minimum is 0, and any decimals will be truncated. A value of 0 will disable edge blurring. Default values is `0`.

-   `zoom`: Sets the zoom level of the map. Requires an argument that is an integer between and including 0 to 4. The map will animate zooming to the specified level. Default value on map load is `1`.

### `map.action( key[, argument ] )`

Performs an action on the map

`key` must be a string corresponding to the action name. Some actions require an additional argument; this may be supplied as the `argument`. The following parameters are supported:

-   `export`: Exports the current map as an image. The default export type is a HTML canvas, but JPG or PNG image types can be specified -   see the export actions below. Requires an argument that is an object literal containing the following parameters. `zoom` which must be an integer between 0 and 4, representing the zoom level at which the map image will be rendered. `done` which must be a callback function accepting one argument. This function will be run when the export is complete. The image or canvas will be supplied to the argument. `tick` which is optional, and should contain a callback function accepting one argument. This function will be run when each tile has been rendered. A value between 0 and 1 will be supplied to the argument, representing what proportion of the total tiles have been rendered.

-   `export.canvas`: Runs the `export` action with the same requirements, but will return a HTML canvas with the map drawn on it.

-   `export.jpg`: Runs the `export` action with the same requirements, but will return a JPG image. Internet Explorer and some versions of Edge may have trouble with this as it uses `toBlob`. If IE or Edge must be supported, use `export.canvas` and convert to a JPG yourself.

-   `export.png`: Runs the `export` action with the same requirements, but will return a PNG image. Internet Explorer and some versions of Edge may have trouble with this as it uses `toBlob`. If IE or Edge must be supported, use `export.canvas` and convert to a PNG yourself.

-   `none`: Stops the current action and sets the map to behave in its default display mode when not being edited.

-   `render`: Forces the map to re-render the current view.

-   `render.terrain`: Forces the map to re-render the terrain layer.

-   `render.textures`: Forces the map to re-render the textures layer.

-   `render.text`: Forces the map to re-render the text layer.

-   `terrain.paint`: Puts the editor in paint mode, to pain one of the predefined terrain textures. Requires an argument that is an object literal containing the following parameters. `color` containing a string value that is one of `blank`, `ocean`, `plains`, `desert`, `water`, `snow`, or `stone`. `brush` containing an integer between 1 and 10 denoting the brush size. The editor will display a brush cursor that can be used to paint strokes of a procedurally generated texture of the colour in `color`. The edges of the painted strokes will be procedurally roughened to simulate natural erosions.

-   `terrain.fill`: Fills the map with a specified terrain texture. Requires an argument that is an object literal containing the following parameter. `color` containing a string value that is one of `blank`, `ocean`, `plains`, `desert`, `water`, `snow`, or `stone`. The map's terrain layer will be completely filled with a procedurally generated texture of the colour in `color`.

-   `text`: Puts the map in text placement mode, allowing specified text to be added to the map. Requires an argument that is an object literal containing the following parameters. `size` (required) containing an integer between 1 and 5, denoting the relative font size. `text` (required) containing a string of maximum 64 characters with the text to place. `rotation` (optional, default `0`) containing a float between 0 and 6.28318530718, denoting the angle in radians to which the text will be rotated. `font` (optional, default `serif`) containing a string with a CSS font face name of a font that has been loaded on the page.

-   `textures.paint`: Puts the editor in paint mode, to pain one of the predefined texture patterns. Requires an argument that is an object literal containing the following parameters. `texture` containing a string value that is one of `waves`, `swamp`, `hatching` or `erase`. `brush` containing an integer between 1 and 10 denoting the brush size. `terrain` containing an array with none, some, or all of the strings `blank`, `ocean`, `plains`, `desert`, `water`, `snow`, or `stone`. The editor will display a brush cursor that can be used to paint strokes of a patterned texture of the type in `texture`. The painted strokes will be cropped to only appear over the terrains specified in the `terrain` array.

## Technologies used

The Atlas Fantasia map depends on a variety of other projects. However a few in particular are fundamental to it. They include:

-   [Leaflet](https://leafletjs.com/)
-   [opentype.js](https://opentype.js.org/)
-   [Polygon Clipping](https://github.com/mfogel/polygon-clipping)
-   [Simplex Noise](#)
