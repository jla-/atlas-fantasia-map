# Changelog
All notable changes to the Atlas Fantasia project will be documented in this file.

## [1.2.3] - 2020-11-02
### Added
-   startTerrain key to init parameter, to specify starting terrain with which map is filled.

## [1.2.2] - 2020-10-24
### Fixed
-   Import map parameter when importing

## [1.2.1] - 2020-10-24
### Fixed
-   External data imports text.

### Changed
-   Reorganised the build scripts, adjusted styling.

## [1.2.0] - 2020-10-19
### Added
-   External data imports on initialisation with defined defaults.

## [1.1.1] - 2020-10-19
### Removed
-   Removed letter spacing option from the text tool.

## [1.1.0] - 2020-10-18
### Changed
-   Rewrote the text SVG generator to use paths built from opentype.js instead of browser fonts.

### Added
-   Support for exporting text as part of the map.

## [1.0.2] - 2020-09-02
### Changed
-   `package.json` additions for NPM and shields for readme.

## [1.0.1] - 2020-09-02
### Added
-   Updated various files, especially `package.json`, for NPM release.

### Changed
-   Code styles fixed for Codacy review.

## [Unreleased]

[Unreleased]: https://gitlab.com/jla-/atlas-fantasia-map/-/network/master
[1.0.1]: https://gitlab.com/jla-/atlas-fantasia-map/-/tags/v1.0.1
[1.0.2]: https://gitlab.com/jla-/atlas-fantasia-map/-/tags/v1.0.2
[1.1.0]: https://gitlab.com/jla-/atlas-fantasia-map/-/tags/v1.1.0
[1.1.1]: https://gitlab.com/jla-/atlas-fantasia-map/-/tags/v1.1.1
[1.2.0]: https://gitlab.com/jla-/atlas-fantasia-map/-/tags/v1.2.0
[1.2.1]: https://gitlab.com/jla-/atlas-fantasia-map/-/tags/v1.2.1
[1.2.2]: https://gitlab.com/jla-/atlas-fantasia-map/-/tags/v1.2.2
[1.2.3]: https://gitlab.com/jla-/atlas-fantasia-map/-/tags/v1.2.3
