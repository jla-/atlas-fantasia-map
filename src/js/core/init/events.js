import debounce from '@/util/debounce'
import errors from '@/util/errors'
import {
    brushPosition,
} from '@/core/regions/brush'
import {
    previewPaint,
    applyPaint,
} from '@/actions/paint/paint'

let s = null,
    ctrl_down = false,
    mouse_down = false,
    hide_overlay = null,
    brush = false,
    bounds = null

const keydown = e => {
    if ( e.keyCode === 16 || e.keyCode === 17 ) {
        s.map.scrollWheelZoom.enable()
        s.container.classList.add( 'zoom' )
        ctrl_down = true
    }
}

const keyup = e => {
    if ( e.keyCode === 16 || e.keyCode === 17 ) {
        s.map.scrollWheelZoom.disable()
        s.container.classList.remove( 'zoom' )
        ctrl_down = false
    }
}

const scroll = e => {
    mouse_down = false
    if ( ctrl_down ) {
        e.preventDefault()
        return false;
    } else {
        s.window.requestAnimationFrame( () => s.overlay.classList.add( 'visible' ) )
        debounce( 'map-scroll', () => {
            clearTimeout( hide_overlay )
            hide_overlay = setTimeout( () => s.overlay.classList.remove( 'visible' ), 500 )
        }, 50 )
    }
}

const mousemove = e => {
    if ( brush )
    {requestAnimationFrame( () => {
        brushPosition( s, e.clientX, e.clientY )

        if ( mouse_down )
            previewPaint( s, e.clientX, e.clientY )
    } )}
}

const mouseenter = e => {
    if ( brush )
        s.brush.classList.add( 'active' )
}

const mouseleave = e => {
    if ( brush )
        s.brush.classList.remove( 'active' )
}

const mousedown = e => {
    if ( e.button === 0 ) {
        mouse_down = true
        previewPaint( s, e.clientX, e.clientY )

        if ( brush )
            s.map.dragging.disable()
    }
}

const mouseup = e => {
    s.map.dragging.enable()
    mouse_down = false

    if ( e.button === 0 )
        applyPaint( s )
}

const deisbleContextMenu = e => {
    e.preventDefault()
    return false
}

export const toggleEnable = ( key, value ) => {
    switch ( key ) {
    case 'brush':
        brush = Boolean( value )
        break
    }
}

export const eventsDestroy = self => {
    self.window.removeEventListener( 'keydown', keydown )
    self.window.removeEventListener( 'keyup', keyup )
    self.window.removeEventListener( 'scroll', scroll )
    self.container.removeEventListener( 'mousemove', mousemove )
    self.container.removeEventListener( 'mouseenter', mouseenter )
    self.container.removeEventListener( 'mouseleave', mouseleave )
    self.container.removeEventListener( 'mousedown', mousedown )
    self.container.removeEventListener( 'mouseup', mouseup )
    self.container.removeEventListener( 'contextmenu', deisbleContextMenu )

    s = null
}

export const eventsInit = self => {
    self.window.addEventListener( 'keydown', keydown )
    self.window.addEventListener( 'keyup', keyup )
    self.window.addEventListener( 'scroll', scroll )
    self.container.addEventListener( 'mousemove', mousemove )
    self.container.addEventListener( 'mouseenter', mouseenter )
    self.container.addEventListener( 'mouseleave', mouseleave )
    self.container.addEventListener( 'mousedown', mousedown )
    self.container.addEventListener( 'mouseup', mouseup )
    self.container.addEventListener( 'contextmenu', deisbleContextMenu )

    s = self
}