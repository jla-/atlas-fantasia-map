import errors from '@/util/errors'
import opentype from 'opentype.js'

export const fontsInit = async ( self, fonts ) => {
    self.fonts = {}

    fonts.forEach( async font => {
        if ( typeof font !== 'object' || typeof font.name !== 'string' || typeof font.path !== 'string' || !font.name || !font.path )
            return

        if ( !self.fonts[ font.name ] )
            self.fonts[ font.name ] = await opentype.load( font.path )
    } )
}