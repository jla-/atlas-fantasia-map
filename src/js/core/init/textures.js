import SimplexNoise from '@/util/vendor/SimplexNoise'
import errors from '@/util/errors'

let textures = {
    base: null,

    waves: null,

    swamp0: null,
    swamp1: null,
    swamp2: null,
    swamp3: null,
    swamp4: null,

    hatching: null,
}

const setSwamp = self => {
    // increments for each zoom level
    let zoom = [
        [ 15, 18 ],
        [ 11, 14 ],
        [ 7, 10 ],
        [ 5, 8 ],
        [ 3, 6 ],
    ]

    for ( let i = zoom.length; i-- > 0; ) { // set each swamp canvas for each zoo=m level
        /* Set up canvas and variables */
        let canvas = document.createElement( 'canvas' ),
            ctx = canvas.getContext( '2d' ),
            width = self.tileWidth,
            height = self.tileHeight,
            incrementx = width / zoom[ i ][ 0 ],
            incx_start = -incrementx / 2,
            incrementy = height / zoom[ i ][ 1 ],
            //    svg = 'data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIyMCIgaGVpZ2h0PSIyMCI+PHBhdGggZmlsbD0iIzAwMCIgZD0iTTguOTUgMy41NXY5LjYxTDEuNjIgNS44My4xMyA3LjMzbDkuMTIgOS4xMmgxLjVsOS4xMi05LjEzLTEuNS0xLjQ5LTcuMzIgNy4zM3YtOS42eiIvPjwvc3ZnPg==',
            svg = 'data:image/svg+xml;utf8,' + encodeURIComponent( `<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20"><path fill="${ self.data.colors_textures.swamp }" d="M8.95 3.55v9.61L1.62 5.83.13 7.33l9.12 9.12h1.5l9.12-9.13-1.5-1.49-7.32 7.33v-9.6z"/></svg>` ),
            img = new Image(),
            x, y, c

        canvas.width = width
        canvas.height = height

        img.onload = () => {
            for ( y = 0, c = 0; y < height; y += incrementy ) {
                for ( x = ( c++ % 2 ? incx_start : 0 ); x < width; x += incrementx )
                    ctx.drawImage( img, x, y, 10, 10 )
            }
        }
        img.src = svg

        /* set as texture */
        textures[ `swamp${ i }` ] = canvas
    }
}

const setHatching = self => {
    /* Set up canvas and variables */
    let canvas = document.createElement( 'canvas' ),
        ctx = canvas.getContext( '2d' ),
        width = self.tileWidth,
        height = self.tileHeight

    canvas.width = width
    canvas.height = height
    ctx.strokeStyle = self.data.colors_textures.hatching
    ctx.lineWidth = 1

    for ( let x = 0; x < Math.max( width, height ) * 2; x += 4 ) {
        ctx.moveTo( 0, x )
        ctx.lineTo( x, 0 )
    }
    ctx.stroke()

    textures.hatching = canvas
}

const setBase = self => {
    /* Set up canvas and variables */
    let canvas = document.createElement( 'canvas' ),
        working = document.createElement( 'canvas' ),
        ctx = canvas.getContext( '2d' ),
        ctx_working = working.getContext( '2d' ),
        width = self.tileWidth,
        height = self.tileHeight,
        simplex = new SimplexNoise(),
        imgdata = ctx_working.getImageData( 0, 0, width, height ),
        imgData = imgdata.data,
        scale_1 = 12960 / width, // magic numbers ~~whooooooo
        scale_2 = 6480 / height,
        i, x, y, t, count

    canvas.width = width
    canvas.height = height

    // Draw Simplex noise
    working.width = width
    working.height = height

    for ( x = 0; x < width; x++ ) {
        for ( y = 0; y < height; y++ ) {
            t = 1 + 0.06 * ( simplex.noise2D( x / scale_1, y / scale_1 ) + simplex.noise2D( x / scale_2, y / scale_2 ) )
            count = ( x + y * width ) * 4

            imgData[ count ] = t * 240 // int is red component
            imgData[ count + 1 ] = t * 210 // int is green component
            imgData[ count + 2 ] = t * 235 // int is blue component
            imgData[ count + 3 ] = 255 // int alpha component
        }
    }

    ctx_working.putImageData( imgdata, 0, 0 )
    ctx.drawImage( working, 0, 0, width, height )

    /* Draw salt and pepper noise */
    working.width = 128
    working.height = 128
    ctx_working.clearRect( 0, 0, 128, 128 );
    let patternData = ctx_working.createImageData( 128, 128 )
    let value
    for ( i = 0, count = 128 * 128 * 4; i < count; i += 4 ) {
        value = ( Math.random() * 255 ) | 0;
        patternData.data[ i ] = value;
        patternData.data[ i + 1 ] = value;
        patternData.data[ i + 2 ] = value;
        patternData.data[ i + 3 ] = 60;
    }
    ctx_working.putImageData( patternData, 0, 0 );
    ctx.fillStyle = ctx.createPattern( working, 'repeat' )
    ctx.fillRect( 0, 0, width, height )

    /* Make seamless */
    working.width = width
    working.height = height
    let w_half = width / 2,
        h_half = height / 2,
        gradient = ctx_working.createRadialGradient(
            w_half, h_half, 0,
            w_half, h_half, Math.min( w_half, h_half ),
        )

    gradient.addColorStop( 0, 'black' )
    gradient.addColorStop( 1, 'transparent' )
    ctx_working.fillStyle = gradient
    ctx_working.fillRect( 0, 0, width, height )
    ctx_working.globalCompositeOperation = 'source-in'
    ctx_working.drawImage( canvas, 0, 0, width, height )

    ctx.drawImage( working, -w_half, -h_half, width, height )
    ctx.drawImage( working, w_half, -h_half, width, height )
    ctx.drawImage( working, w_half, h_half, width, height )
    ctx.drawImage( working, -w_half, h_half, width, height )

    /* set as base texture */
    textures.base = canvas
}

export const texturesInit = self => {
    setBase( self )
    setSwamp( self )
    setHatching( self )
}

export const texturesUpdate = self => {
    setSwamp( self )
    setHatching( self )
}

export default textures
