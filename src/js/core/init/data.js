import polygon from '@/util/polygon'

export const dataDestroy = self => {
    self.data = null
}

export const dataInit = ( self, imported, startTerrain ) => {
    let defaultValues = [
        [ 'colors', {} ],
        [ 'colors.blank', '#000000' ],
        [ 'colors.ocean', '#416091' ],
        [ 'colors.grassland', '#268221' ],
        [ 'colors.desert', '#efdd77' ],
        [ 'colors.water', '#4996b2' ],
        [ 'colors.snow', '#dae4e8' ],
        [ 'colors.stone', '#484d57' ],

        [ 'colors_textures', {} ],
        [ 'colors_textures.waves', '#273c59' ],
        [ 'colors_textures.swamp', '#293d28' ],
        [ 'colors_textures.hatching', '#000000' ],

        [ 'options', {} ],
        [ 'options.terrain_stroke', 2 ],
        [ 'options.terrain_blur', 0 ],

        [ 'terrain', {} ],
        [ 'terrain.blank', [] ],
        [ 'terrain.ocean', [] ],
        [ 'terrain.grassland', [] ],
        [ 'terrain.desert', [] ],
        [ 'terrain.water', [] ],
        [ 'terrain.snow', [] ],
        [ 'terrain.stone', [] ],

        [ 'textures', {} ],
        [ 'textures.waves', [] ],
        [ 'textures.swamp', [] ],
        [ 'textures.hatching', [] ],

        [ 'text', {} ],
    ]

    if ( startTerrain !== 'blank' && startTerrain !== 'ocean' && startTerrain !== 'grassland' && startTerrain !== 'desert' && startTerrain !== 'water' && startTerrain !== 'snow' && startTerrain !== 'stone' )
        startTerrain = 'blank'

    for ( let i = defaultValues.length; i-- > 0; ) {
        if ( defaultValues[ i ][ 0 ] === `terrain.${ startTerrain }` ) {
            defaultValues[ i ][ 1 ] = polygon( [
                [ self.maxBounds[ 0 ][ 0 ], self.maxBounds[ 0 ][ 1 ] ],
                [ self.maxBounds[ 0 ][ 0 ], self.maxBounds[ 1 ][ 1 ] ],
                [ self.maxBounds[ 1 ][ 0 ], self.maxBounds[ 1 ][ 1 ] ],
                [ self.maxBounds[ 1 ][ 0 ], self.maxBounds[ 0 ][ 1 ] ],
                [ self.maxBounds[ 0 ][ 0 ], self.maxBounds[ 0 ][ 1 ] ],
            ] )
            break
        }
    }

    self.data = {}
    if ( typeof imported !== 'object' )
        imported = {}

    for ( let data of defaultValues ) {
        let properties = data[ 0 ].split( '.' ),
            defaultValue = data[ 1 ]

        if ( properties.length === 1 )
            self.data[ properties[ 0 ] ] = {}

        else {
            let importedValue = typeof imported[ properties[ 0 ] ] === 'object' ? imported[ properties[ 0 ] ][ properties[ 1 ] ] : undefined
            if ( typeof importedValue === typeof defaultValue )
                self.data[ properties[ 0 ] ][ properties[ 1 ] ] = importedValue
            else
                self.data[ properties[ 0 ] ][ properties[ 1 ] ] = defaultValue
        }
    }

    if ( typeof imported.text === 'object' ) {
        for ( let id in imported.text ) {
            try {
                self.data.text[ id ] = {
                    el: '' + imported.text[ id ].el,
                    bounds: [
                        [ imported.text[ id ].bounds[ 0 ][ 0 ], imported.text[ id ].bounds[ 0 ][ 1 ] ],
                        [ imported.text[ id ].bounds[ 1 ][ 0 ], imported.text[ id ].bounds[ 1 ][ 1 ] ],
                    ],
                }
            } catch ( e ) {}
        }
    }

    return self
}
