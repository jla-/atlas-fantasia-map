import PolygonClipping from 'polygon-clipping'
import polygon from '@/util/polygon'
import textures from '@/core/init/textures'

let polyBounds,
    coordinates,
    swlat,
    swlng,
    h, w,
    x, y, z,
    last_z

export default ( self, ctx, bounds ) => {
    polyBounds = polygon( [
        [ bounds._southWest.lng, bounds._southWest.lat ],
        [ bounds._southWest.lng, bounds._northEast.lat ],
        [ bounds._northEast.lng, bounds._northEast.lat ],
        [ bounds._northEast.lng, bounds._southWest.lat ],
        [ bounds._southWest.lng, bounds._southWest.lat ],
    ] )
    swlat = bounds._southWest.lat
    swlng = bounds._southWest.lng
    h = self.tileWidth / Math.abs( bounds._northEast.lat - bounds._southWest.lat )
    w = self.tileHeight / Math.abs( bounds._northEast.lng - bounds._southWest.lng )

    Object.keys( self.data.textures ).forEach( texture => { // draw each set of texture coordinates
        coordinates = PolygonClipping.intersection( // clip coords to tile bounds
            polyBounds,
            self.data.textures[ texture ],
        )

        for ( x = coordinates.length; x-- > 0; ) {
            ctx.beginPath()
            for ( y = coordinates[ x ].length; y-- > 0; ) {
                last_z = coordinates[ x ][ y ].length - 1
                if ( last_z >= 0 ) {
                    ctx.moveTo(
                        Math.floor( ( coordinates[ x ][ y ][ last_z ][ 0 ] - swlng ) * w ),
                        Math.floor( ( coordinates[ x ][ y ][ last_z ][ 1 ] - swlat ) * h ),
                    )
                    for ( z = coordinates[ x ][ y ].length; z-- > 0; ) {
                        ctx.lineTo(
                            Math.floor( ( coordinates[ x ][ y ][ z ][ 0 ] - swlng ) * w ),
                            Math.floor( ( coordinates[ x ][ y ][ z ][ 1 ] - swlat ) * h ),
                        )
                    }
                    ctx.closePath()
                }
            }

            ctx.save()
            ctx.clip( ( coordinates[ x ].length > 1 ? 'evenodd' : 'nonzero' ) )
            if ( textures[ texture ] )
                ctx.drawImage( textures[ texture ], 0, 0 )
            else
                ctx.drawImage( textures[ texture + self.getZoom() ], 0, 0 )
            ctx.restore()
        }
    } )
}