import {
    makeAsync,
} from '@/util/helpers'
import renderTerrain from './terrain/render'
import renderTextures from './textures/render'
import renderText from './text/render'

export default ( self, bounds, layer, done ) => {
    let tile,
        ctx
    if ( layer == 'text' )
        tile = document.createElement( 'div' )
    else {
        tile = document.createElement( 'canvas' )
        tile.width = self.tileWidth
        tile.height = self.tileHeight
        ctx = tile.getContext( '2d' )
    }

    /* Asynchronouse tile render */
    setTimeout( () => {
        if (
            bounds._northEast.lng <= self.maxBounds[ 1 ][ 0 ] &&
            bounds._northEast.lat <= self.maxBounds[ 1 ][ 1 ] &&
            bounds._southWest.lng >= self.maxBounds[ 0 ][ 0 ] &&
            bounds._southWest.lat >= self.maxBounds[ 0 ][ 1 ]
        )
        {switch ( layer ) {
        case 'terrain':
            renderTerrain( self, ctx, bounds )
            break

        case 'textures':
            renderTextures( self, ctx, bounds )
            break

        case 'text':
            renderText( self, tile, bounds )
            break
        }}

        if ( self.debug && layer === 'terrain' ) {
            ctx.strokeStyle = 'red'
            ctx.lineWidth = 1
            ctx.strokeRect( 0, 0, self.tileWidth, self.tileHeight )
            ctx.font = '10px serif';
            let lines = JSON.stringify( bounds, null, 4 ).split( '\n' )
            lines.forEach( ( line, index ) => ctx.fillText( line, 10, 10 + index * 12 ) )
        }

        done( null, tile )
    }, 0 )

    return tile
}