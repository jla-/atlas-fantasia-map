const intersect = ( a, b ) => {
    return (
        a.left <= b.right &&
        b.left <= a.right &&
        a.top <= b.bottom &&
        b.top <= a.bottom
    )
}

export default ( self, tile, bounds ) => {
    let bounds_tile, bounds_text, x, y, w, h,
        scale = self.tileWidth / Math.abs( bounds._northEast.lng - bounds._southWest.lng )

    tile.style.overflow = 'hidden'

    bounds_tile = {
    // The labels are all wrong but this is because the custom CRS makes the
    // south east corner be labeled _northEast and the north west corner be
    // labelled _southWest
        left: bounds._southWest.lng,
        right: bounds._northEast.lng,
        top: bounds._southWest.lat,
        bottom: bounds._northEast.lat,
    }

    Object.keys( self.data.text ).forEach( id => {

        bounds_text = {
            left: self.data.text[ id ].bounds[ 1 ][ 0 ],
            right: self.data.text[ id ].bounds[ 0 ][ 0 ],
            top: self.data.text[ id ].bounds[ 0 ][ 1 ],
            bottom: self.data.text[ id ].bounds[ 1 ][ 1 ],
        }

        if ( intersect( bounds_tile, bounds_text ) && self.textEls[ id ] ) {
            let svg = self.textEls[ id ].cloneNode( true )
            x = scale * ( bounds_text.left - bounds_tile.left )
            y = scale * ( bounds_text.top - bounds_tile.top )
            w = scale * ( bounds_text.right - bounds_text.left )
            h = scale * ( bounds_text.bottom - bounds_text.top )
            svg.style.left = `${ x }px`
            svg.style.top = `${ y }px`
            svg.style.width = `${ w }px`
            svg.style.height = `${ h }px`
            tile.appendChild( svg )
        }
    } )
}