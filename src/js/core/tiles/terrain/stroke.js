let swlat, // declare first so don't have to allocate/gc memory on each tile render
    swlng,
    h, w,
    x, y, z, i

export default ( self, ctx, bounds, coordinates ) => {
    if ( coordinates ) {
        swlat = bounds._southWest.lat // faster reading raw value than reading obj properties
        swlng = bounds._southWest.lng
        h = self.tileHeight / Math.abs( bounds._northEast.lat - bounds._southWest.lat )
        w = self.tileWidth / Math.abs( bounds._northEast.lng - bounds._southWest.lng )

        ctx.beginPath()
        for ( x = coordinates.length; x-- > 0; ) {
            for ( y = coordinates[ x ].length; y-- > 0; ) {
                for ( z = coordinates[ x ][ y ].length; z-- > 0; ) {

                    i = ( z > 0 ? z - 1 : coordinates[ x ][ y ].length - 1 )
                    if ( // check if not horizontal or vertical and on the tile boundary
                        (
                            coordinates[ x ][ y ][ z ][ 0 ] !== bounds._southWest.lng ||
                            coordinates[ x ][ y ][ i ][ 0 ] !== bounds._southWest.lng
                        ) && (
                            coordinates[ x ][ y ][ z ][ 0 ] !== bounds._northEast.lng ||
                            coordinates[ x ][ y ][ i ][ 0 ] !== bounds._northEast.lng
                        ) && (
                            coordinates[ x ][ y ][ z ][ 1 ] !== bounds._southWest.lat ||
                            coordinates[ x ][ y ][ i ][ 1 ] !== bounds._southWest.lat
                        ) && (
                            coordinates[ x ][ y ][ z ][ 1 ] !== bounds._northEast.lat ||
                            coordinates[ x ][ y ][ i ][ 1 ] !== bounds._northEast.lat
                        )
                    ) {
                        ctx.moveTo(
                            Math.floor( ( coordinates[ x ][ y ][ z ][ 0 ] - swlng ) * w ),
                            Math.floor( ( coordinates[ x ][ y ][ z ][ 1 ] - swlat ) * h ),
                        )
                        ctx.lineTo(
                            Math.floor( ( coordinates[ x ][ y ][ i ][ 0 ] - swlng ) * w ),
                            Math.floor( ( coordinates[ x ][ y ][ i ][ 1 ] - swlat ) * h ),
                        )
                    }

                }
                ctx.stroke()
            }
        }
    }
}