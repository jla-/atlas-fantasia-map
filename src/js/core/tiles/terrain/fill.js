let last_z, // declare first so don't have to allocate/gc memory on each tile render
    x, y, z,
    swlat,
    swlng,
    h, w

export default ( self, ctx, bounds, coordinates, terrain, blur, scaledBlur ) => {
    swlat = bounds._southWest.lat - scaledBlur
    swlng = bounds._southWest.lng - scaledBlur
    h = ( self.tileWidth + blur * 2 ) / Math.abs( scaledBlur * 2 + bounds._northEast.lat - bounds._southWest.lat )
    w = ( self.tileHeight + blur * 2 ) / Math.abs( scaledBlur * 2 + bounds._northEast.lng - bounds._southWest.lng )

    for ( x = coordinates.length; x-- > 0; ) {
        ctx.beginPath()
        for ( y = coordinates[ x ].length; y-- > 0; ) {
            last_z = coordinates.length - 1
            if ( coordinates[ x ][ y ][ last_z ] ) {
                // See https://stackoverflow.com/a/7058606/1427563 for when implementing this as a curve
                ctx.moveTo(
                    Math.floor( ( coordinates[ x ][ y ][ last_z ][ 0 ] - swlng ) * w ),
                    Math.floor( ( coordinates[ x ][ y ][ last_z ][ 1 ] - swlat ) * h ),
                )
                for ( z = coordinates[ x ][ y ].length; z-- > 0; ) {
                    ctx.lineTo(
                        Math.floor( ( coordinates[ x ][ y ][ z ][ 0 ] - swlng ) * w ),
                        Math.floor( ( coordinates[ x ][ y ][ z ][ 1 ] - swlat ) * h ),
                    )
                }
                ctx.closePath()
            }
        }
        ctx.save()
        ctx.clip( ( coordinates[ x ].length > 1 ? 'evenodd' : 'nonzero' ) )
        ctx.fillStyle = self.data.colors[ terrain ]
        ctx.strokeStyle = self.data.colors[ terrain ]
        ctx.lineWidth = 1
        ctx.lineCap = 'round'
        ctx.fill()
        ctx.stroke()
        ctx.restore()
    }
}