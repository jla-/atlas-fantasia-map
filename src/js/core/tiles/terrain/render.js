import PolygonClipping from 'polygon-clipping'
import stackBlur from '@/util/vendor/StackBlur'
import polygon from '@/util/polygon'
import textures from '@/core/init/textures'
import fill from './fill'
import stroke from './stroke'

let blank, // declare first so don't have to allocate/gc memory on each tile render
    water,
    ocean,
    blur,
    scaledBlur,
    scaledBounds,
    tmpWidth,
    tmpHeight,
    coordinates

export default ( self, ctx, bounds ) => {
    let tmpCanvas = document.createElement( 'canvas' ),
        tmpCtx = tmpCanvas.getContext( '2d' )

    blur = self.data.options.terrain_blur * 10
    scaledBlur = blur * Math.abs( bounds._northEast.lng - bounds._southWest.lng ) / self.tileWidth
    scaledBounds = polygon( [ // tile bounds are expanded by the blur level amount
        [ bounds._southWest.lng - scaledBlur, bounds._southWest.lat - scaledBlur ],
        [ bounds._southWest.lng - scaledBlur, bounds._northEast.lat + scaledBlur ],
        [ bounds._northEast.lng + scaledBlur, bounds._northEast.lat + scaledBlur ],
        [ bounds._northEast.lng + scaledBlur, bounds._southWest.lat - scaledBlur ],
        [ bounds._southWest.lng - scaledBlur, bounds._southWest.lat - scaledBlur ],
    ] )
    tmpWidth = self.tileWidth + blur * 2 // working canvas, size is the same as expanded tile bounds
    tmpHeight = self.tileHeight + blur * 2
    blank = []
    water = []
    ocean = []

    tmpCanvas.width = tmpWidth
    tmpCanvas.height = tmpHeight

    Object.keys( self.data.terrain ).forEach( terrain => { // draw each set of terrain coordinates
        coordinates = PolygonClipping.intersection( // clip coords to expanded tile bounds
            scaledBounds,
            self.data.terrain[ terrain ],
        )
        fill( self, tmpCtx, bounds, coordinates, terrain, blur, scaledBlur )
        if ( terrain === 'blank' )
            blank = coordinates
        if ( terrain === 'ocean' )
            ocean = coordinates
        if ( terrain === 'water' )
            water = coordinates
    } )

    if ( self.data.options.terrain_blur ) // blur if level is greater than 0
        stackBlur( tmpCtx, tmpWidth, tmpHeight, blur )

    ctx.drawImage( textures.base, 0, 0 ) // crop blurred canvas to actual tile size and copy to the real tile canvas
    ctx.globalAlpha = 0.5
    ctx.drawImage( tmpCanvas, blur, blur, self.tileWidth, self.tileHeight, 0, 0, self.tileWidth, self.tileHeight )
    ctx.globalAlpha = 1

    if ( self.data.options.terrain_stroke ) { // outline if width > 0
        ctx.strokeStyle = '#000000'
        ctx.lineWidth = self.data.options.terrain_stroke
        ctx.lineCap = 'round'
        stroke( self, ctx, bounds, blank )
        if ( water.length || ocean.length )
            stroke( self, ctx, bounds, PolygonClipping.union( water, ocean ) )
    }
}