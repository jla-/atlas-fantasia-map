import errors from '@/util/errors'
import {
    hexToRgb,
} from '@/util/helpers'
import {
    toggleEnable,
} from '@/core/init/events'

const clearBrush = self => {
    /* Disable events */
    self.container.classList.remove( 'brush' )
    toggleEnable( 'brush', false )

    /* Set brush styles */
    self.brush.className = 'brush'
    self.brush.style.backgroundColor = ''
    self.brush.style.width = '0'
    self.brush.style.height = '0'
    self.brush.innerHTML = ''
    return self
}

const setBrush = ( self, size ) => {
    /* Enable events */
    self.container.classList.add( 'brush' )
    toggleEnable( 'brush', true )

    /* Set brush styles */
    self.brush.style.width = `${ size * 20 }px`
    self.brush.style.height = `${ size * 20 }px`
    self.brush.style.top = `${ ( self.height - size ) / 2 }px`
    self.brush.style.left = `${ ( self.width - size ) / 2 }px`
    self.brush.classList.add( 'active' )
}

const setPaint = ( self, options ) => {
    setBrush( self, options.size )
    self.brush.style.backgroundColor = `rgba(${ options.color.r },${ options.color.g }, ${ options.color.b },0.5)`
    self.brush.classList.add( 'paint' )
    return self
}

const setShape = self => {
    setBrush( self, 3 )
    self.brush.classList.add( 'shape' )
    return self
}

const setText = ( self, svg ) => {
    setBrush( self, 0 )
    self.brush.classList.add( 'text' )
    self.brush.appendChild( svg )
    return self
}

export const brushInit = self => {
    let brush = document.createElement( 'div' )
    brush.className = 'brush'
    self.brush = brush
    self.brushWrap.className = 'brush-el'
    self.brushWrap.appendChild( brush )
}

export const brushPosition = ( self, fixed_x, fixed_y ) => {
    let brush_bounds = self.brush.getBoundingClientRect(),
        wrap_bounds = self.brushWrap.getBoundingClientRect()

    self.brush.style.left = `${ fixed_x - wrap_bounds.left - brush_bounds.width / 2 }px`
    self.brush.style.top = `${ fixed_y - wrap_bounds.top - brush_bounds.height / 2 }px`
}

export default ( self, type, options ) => {
    if ( !self.brush )
        return errors.throw( 'Brush is not initialised.' )

    self.brush.className = 'brush'
    self.brush.innerHTML = ''

    switch ( type ) {
    case 'paint':
        if ( self.data.colors[ options.color ] )
            options.color = hexToRgb( self.data.colors[ options.color ] )
        else
            options.color = hexToRgb( options.color )
        if ( !options.color )
            return errors.throw( 'Must use a valid colour name or hex value' )

        options.size = Math.min( 10, Math.max( 0, parseInt( options.size, 10 ) ) )
        return setPaint( self, options )

    case 'shape':
        return setShape( self )

    case 'text':
        return setText( self, options )
    }

    return clearBrush( self )
}