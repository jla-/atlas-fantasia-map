import {
    init as terrainInit,
} from '@/actions/terrain/paint'
import {
    init as texturesInit,
} from '@/actions/textures/paint'
import {
    init as featuresRiversInit,
} from '@/actions/features.rivers/shape'
import {
    init as textInit,
} from '@/actions/text/paint'

export const previewInit = self => {
    self.preview.width = self.width
    self.preview.height = self.height
    self.preview.className = 'preview-el'

    terrainInit( self )
    featuresRiversInit( self )
    texturesInit( self )
    textInit( self )
}