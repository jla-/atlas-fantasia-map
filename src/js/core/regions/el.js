import errors from '@/util/errors.js'

export const elInit = self => {
    if ( !self.container )
        errors.fatal( 'Invalid DOM element' )

    self.container.innerHTML = ''
    self.container.classList.add( 'atlas-fantasia-map' )

    if ( !self.container.offsetHeight )
        self.container.style.height = `${ self.container.offsetWidth * 0.58 }px`

    self.width = self.container.offsetWidth
    self.height = self.container.offsetHeight

    self.tileWidth = 256
    self.tileHeight = 256

    let el = document.createElement( 'div' ),
        overlay = document.createElement( 'div' ),
        brush = document.createElement( 'div' ),
        preview = document.createElement( 'canvas' )

    self.container.appendChild( el )
    self.container.appendChild( overlay )
    self.container.appendChild( brush )
    self.container.appendChild( preview )

    el.className = 'map-el'

    self.el = el
    self.overlay = overlay
    self.brushWrap = brush
    self.preview = preview
}