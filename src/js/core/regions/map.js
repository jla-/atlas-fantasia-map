import L from 'leaflet'
import errors from '@/util/errors'
import tiles from '@/core/tiles/tiles'

let terrain,
    textures,
    text

export const mapDestroy = self => {
    self.map.off()
    self.map.remove()
}

export const mapInit = self => {
    if ( !self.el )
        errors.fatal( 'Initialisation failed.' )

    L.CRS.AF = L.extend( {}, L.CRS.Simple, {
    // Extend Leaflet's Simple CRS so coordinates are from top left.
    // This messes up Leaflet's internal bound scheme. The map bounds makes the
    // south east corner be labeled _northEast and the north west corner be
    // labelled _southWest
        transformation: new L.Transformation( 1, 0, 1, 0 ),
    } )

    self.map = L.map( self.el, {
        attributionControl: false,
        zoomControl: false,
        boxZoom: false,
        maxBounds: self.maxBounds,
        // fadeAnimation: false,
        crs: L.CRS.AF,
    } )

    /* Default view */
    self.map.fitBounds( self.maxBounds )
    let minZoom = self.map.getZoom(),
        maxZoom = minZoom + 4
    self.map.setMinZoom( minZoom )
    self.map.setMaxZoom( maxZoom )
    self.map.zoomIn()

    self.getZoom = () => self.map.getZoom() - minZoom
    self.setZoom = zoom => self.map.setZoom( zoom + minZoom )

    /* Zooming and panning */
    self.map.scrollWheelZoom.disable()

    /* Grid layer for terrain */
    L.GridLayer.Terrain = L.GridLayer.extend( {
        createTile: function( coords, done ) {
            let bounds = this._tileCoordsToBounds( coords ),
                tile = tiles( self, bounds, 'terrain', done )
            return tile
        },
    } )

    /* Grid layer for terrain */
    L.GridLayer.Textures = L.GridLayer.extend( {
        createTile: function( coords, done ) {
            let bounds = this._tileCoordsToBounds( coords ),
                tile = tiles( self, bounds, 'textures', done )
            return tile
        },
    } )

    /* Grid layer for text */
    L.GridLayer.Text = L.GridLayer.extend( {
        createTile: function( coords, done ) {
            let bounds = this._tileCoordsToBounds( coords ),
                tile = tiles( self, bounds, 'text', done )
            return tile
        },
    } )

    L.gridLayer.terrain = opts => new L.GridLayer.Terrain( opts )
    L.gridLayer.textures = opts => new L.GridLayer.Textures( opts )
    L.gridLayer.text = opts => new L.GridLayer.Text( opts )

    terrain = L.gridLayer.terrain()
    textures = L.gridLayer.textures()
    text = L.gridLayer.text()

    self.map.addLayer( terrain )
    self.map.addLayer( textures )
    self.map.addLayer( text )

    self.map.on( 'zoomend', function() {
        mapRender( self, 'textures' )
    } )
}

export const mapRender = ( self, layer ) => {
    switch ( layer ) {
    case 'terrain':
        terrain.redraw()
        break

    case 'textures':
        textures.redraw()
        break

    case 'text':
        text.redraw()
        break

    default:
        self.map.invalidateSize()
        self.map._resetView( self.map.getCenter(), self.map.getZoom(), true )
    }

}