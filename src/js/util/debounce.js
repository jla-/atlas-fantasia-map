import errors from '@/util/errors'

var list = {}

export default ( key, fn, time, context ) => {

    if ( typeof key !== 'string' )
        return errors.throw( 'Must supply a valid key to debounce.' )

    if ( typeof fn !== 'function' )
        return errors.throw( 'Must supply a value function to debounce.' )

    if ( !time )
        time = 100

    if ( !context )
        context = this

    if ( list[ key ] )
        clearTimeout( list[ key ] )

    list[ key ] = setTimeout( () => {
        fn.call( context )
        list[ key ] = null
    }, time )
}