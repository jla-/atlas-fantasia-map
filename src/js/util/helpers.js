export const hexToRgb = hex => {
    if ( typeof hex !== 'string' || !hex )
        return null

    hex = hex.replace( /[^0-9A-F]/gi, '' )
    let bigint = parseInt( hex, 16 ),
        r = ( bigint >> 16 ) & 255,
        g = ( bigint >> 8 ) & 255,
        b = bigint & 255

    return {
        r: r,
        g: g,
        b: b,
    }
}

export const sleep = ms => new Promise( r => setTimeout( r, ms ) )

export const makeAsync = cb => setTimeout( cb, 100 )