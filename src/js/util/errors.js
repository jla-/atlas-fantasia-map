const error = {
    fatal: msg => {
        if ( typeof msg !== 'string' || !msg )
            msg = 'A fatal error occurred.'

        throw new Error( `Atlas Fantasia map error: ${ msg }` )
    },

    throw: msg => {
        if ( typeof msg !== 'string' || !msg )
            msg = 'An error occurred.'

        console.error( `Atlas Fantasia map error: ${ msg }` )
        return new Error( msg )
    },
}

export default error