export default points => {

    if ( !Array.isArray( points ) )
        points = []

    if ( !Array.isArray( points[ 0 ] ) )
        points = [ points ]

    if ( !Array.isArray( points[ 0 ][ 0 ] ) )
        points = [ points ]

    if ( !Array.isArray( points[ 0 ][ 0 ][ 0 ] ) )
        points = [ points ]

    if ( isNaN( +points[ 0 ][ 0 ][ 0 ][ 0 ] ) )
        points = []

    return points
}