import errors from '@/util/errors.js'
import {
    elInit,
} from '@/core/regions/el'
import {
    fontsInit,
} from '@/core/init/fonts'
import {
    dataInit,
} from '@/core/init/data'
import {
    brushInit,
} from '@/core/regions/brush'
import {
    eventsInit,
} from '@/core/init/events'
import {
    mapInit,
} from '@/core/regions/map'
import {
    overlayInit,
} from '@/core/regions/overlay'
import {
    previewInit,
} from '@/core/regions/preview'
import {
    texturesInit,
} from '@/core/init/textures'

export default async ( self, selector, config ) => {

    let el

    if ( !config || typeof config !== 'object' )
        config = {}

    /* Get selected element and check if it exists */
    if ( typeof selector === 'string' )
        el = document.querySelector( selector )
    else
        el = selector

    if (
        !( el instanceof Element ) &&
        !( el instanceof HTMLDocument )
    )
        return errors.fatal( 'Invalid DOM element or selector passed to the initialiser' )

    self.container = el

    self.maxBounds = [
        [ 0, 0 ],
        [ 1, 1 ],
    ]

    /* Run initialisation functions */
    elInit( self )
    await fontsInit( self, config.fonts )
    dataInit( self, config.data, config.startTerrain )
    texturesInit( self )
    brushInit( self )
    overlayInit( self )
    previewInit( self )
    mapInit( self )
    eventsInit( self )

    /* Set the public el as read-only */
    delete self.public.el
    Object.defineProperty( self.public, 'el', {
        value: el,
        writable: true, // I'd make this false but there's a rare bug that sometimes gets thrown on destroy()
    } )

    /* completed initialising */
    self.initialised = true
    return self.public
}
