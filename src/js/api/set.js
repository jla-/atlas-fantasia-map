import errors from '@/util/errors';
import {
    texturesUpdate,
} from '@/core/init/textures'
import {
    mapRender,
} from '@/core/regions/map'

export default ( self, key, arg ) => {

    if ( !self.initialised )
        return errors.fatal( 'Not initialised.' )

    if ( typeof key !== 'string' || !key )
        return errors.throw( 'Must provide a valid key.' )

    if ( typeof arg === 'undefined' )
        return errors.throw( 'Must provide a valid value' )

    switch ( key ) {

    case 'colors':
    case 'colors.terrain':
        /* Set terrain colour names and hexadecimal values */
        if ( typeof arg !== 'object' || !arg )
            return errors.throw( 'Must provide a valid object of colours' )

        for ( let color in self.data.colors ) {
            if ( typeof arg[ color ] === 'string' )
                self.data.colors[ color ] = arg[ color ]

        }
        break

    case 'colors.textures':
        /* Set texture colour names and hexadecimal values */
        if ( typeof arg !== 'object' || !arg )
            return errors.throw( 'Must provide a valid object of colours' )

        for ( let color in self.data.colors_textures ) {
            if ( typeof arg[ color ] === 'string' )
                self.data.colors_textures[ color ] = arg[ color ]

        }
        texturesUpdate( self )
        break

    case 'terrain.outline':
        /* Set thickness of outline around terrain */
        if ( isNaN( +arg ) )
            return errors.throw( 'Must provide an integer as the outline thickness, or 0 for none' )

        self.data.options.terrain_stroke = Math.max( 0, Math.min( 5, Math.floor( +arg ) ) )
        break

    case 'terrain.blur':
        /* Set amount of blurring between different terrain */
        if ( isNaN( +arg ) )
            return errors.throw( 'Must provide an integer as the blur level, or 0 for none' )

        self.data.options.terrain_blur = Math.max( 0, Math.min( 5, Math.floor( +arg ) ) )
        break

    case 'zoom':
        /* Set the map zoom level, between 0 and 4 */
        if ( isNaN( +arg ) )
            return errors.throw( 'Must provide an integer as the zoom level' )

        self.setZoom( Math.max( 0, Math.min( 4, Math.floor( +arg ) ) ) )
        break

    default:
        return errors.throw( 'Must provide a valid key.' )

    }

    return self.public
}
