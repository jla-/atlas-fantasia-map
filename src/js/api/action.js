import errors from '@/util/errors'
import brush from '@/core/regions/brush'
import exporter from '@/actions/export/export'
import textSvg from '@/actions/text/svg'
import {
    mapRender,
} from '@/core/regions/map'
import {
    beginPaint,
} from '@/actions/paint/paint'
import fillTerrain from '@/actions/terrain/fill'

export default ( self, key, arg ) => {

    if ( !self.initialised )
        return errors.fatal( 'Not initialised.' )

    if ( typeof key !== 'string' || !key )
        return errors.throw( 'Must provide a valid action.' )

    /* Clear brush by default */
    brush( self )

    switch ( key ) {

    case 'none':
    /* No action */
        self.action = 'none'
        break

    case 'export':
    case 'export.canvas':
    case 'export.jpg':
    case 'export.png':
    /* Export an image */
        if (
            typeof arg !== 'object' ||
                typeof arg.done !== 'function' ||
                isNaN( +arg.zoom )
        )
            return errors.throw( 'Argument must contain valid function to run when complete and a valid zoom level at which to export.' )

        self.action = 'none'
        let type = key.indexOf( 'jpg' ) !== -1 ? 'jpg' : key.indexOf( 'png' ) !== -1 ? 'png' : 'canvas'
        arg.zoom = Math.max( 0, Math.min( 4, arg.zoom ) )
        exporter( self, type, arg.zoom, arg.done, arg.tick )
        break

    case 'features.shape.rivers':
    /* Draw river shape */
        if (
            typeof arg !== 'object' ||
                isNaN( +arg.size ) ||
                isNaN( +arg.tapering )
        )
            return errors.throw( 'Argument must contain valid size and tapering properties.' )

        self.action = 'features.shape.rivers'
        arg.size = Math.max( 1, Math.min( 10, arg.size ) )
        arg.tapering = Math.max( 1, Math.min( 10, arg.tapering ) )
        brush( self, 'shape', arg.size )
        beginPaint( self, arg )
        break

    case 'render':
    /* Redraw the map */
        self.action = 'none'
        mapRender( self )
        break

    case 'render.terrain':
    /* Redraw the terrain layer */
        self.action = 'none'
        mapRender( self, 'terrain' )
        break

    case 'render.textures':
    /* Redraw the textures layer */
        self.action = 'none'
        mapRender( self, 'textures' )
        break

    case 'terrain.fill':
    /* Fill terrain */
        if (
            typeof arg !== 'object' ||
                typeof arg.color !== 'string'
        )
            return errors.throw( 'Argument must contain valid colour property.' )

        self.action = 'none'
        fillTerrain( self, arg.color )
        break

    case 'terrain.paint':
    /* Paint terrain */
        if (
            typeof arg !== 'object' ||
                typeof arg.color !== 'string' ||
                isNaN( +arg.brush )
        )
            return errors.throw( 'Argument must contain valid colour and brush size properties.' )

        self.action = 'terrain.paint'
        arg.brush = Math.max( 1, Math.min( 10, arg.brush ) )
        brush( self, 'paint', {
            size: arg.brush,
            color: arg.color,
        } )
        beginPaint( self, arg )
        break

    case 'text':
    /* Place text */
        if (
            typeof arg !== 'object' ||
                isNaN( +arg.size ) ||
                typeof arg.color !== 'string' ||
                !arg.color ||
                typeof arg.text !== 'string' ||
                !arg.text
        )
            return errors.throw( 'Argument must contain text, color, and size properties.' )

        self.action = 'text'
        arg.text = arg.text.length > 64 ? arg.text.substring( 0, 64 ) : arg.text
        arg.size = Math.max( 1, Math.min( 5, +arg.size ) )
        arg.rotation = Math.max( 0, Math.min( 6.28318530718, parseFloat( arg.rotation ) || 0 ) )
        arg.font = typeof arg.font === 'string' && arg.font ? arg.font : 'serif'

        let svg = textSvg( self, arg )
        arg.svg = svg
        brush( self, 'text', svg )
        beginPaint( self, arg )
        break

    case 'textures.paint':
    /* Paint textures */
        if (
            typeof arg !== 'object' ||
                isNaN( +arg.brush )
        )
            return errors.throw( 'Argument must contain valid brush size properties.' )

        self.action = 'textures.paint'
        arg.brush = Math.max( 1, Math.min( 10, arg.brush ) )
        brush( self, 'paint', {
            size: arg.brush,
            color: '#000000',
        } )
        beginPaint( self, arg )
        break

    default:
        return errors.throw( 'Must provide a valid action.' )

    }

    return self.public
}