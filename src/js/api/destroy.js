import errors from '@/util/errors'
import {
    dataDestroy,
} from '@/core/init/data'
import {
    mapDestroy,
} from '@/core/regions/map'
import {
    eventsDestroy,
} from '@/core/init/events'

export default self => {
    try {
        dataDestroy( self )
        eventsDestroy( self )
        mapDestroy( self )
    } catch ( e ) {
        errors.throw( 'There was an error destroying the map.' )
    }
    self = null
    return null
}