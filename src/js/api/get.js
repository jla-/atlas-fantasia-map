import errors from '@/util/errors'

export default ( self, key, arg ) => {

    if ( !self.initialised )
        return errors.fatal( 'Not initialised.' )

    if ( typeof key !== 'string' || !key )
        return errors.throw( 'Must provide a valid key.' )

    switch ( key ) {

    case 'action':
    /* The current action being undertaken by the map editor */
        return self.action

    case 'color':
    case 'color.terrain':
    /* The hexadecimal colour value of a terrain colour name (ocean, desert, snow, etc) */
        if ( typeof arg !== 'string' || !arg || typeof self.data.colors[ arg ] !== 'string' )
            return errors.throw( 'Must provide a valid colour.' )

        return self.data.colors[ arg ]

    case 'color.textures':
    /* The hexadecimal colour value of a texture colour name (waves, swamp, etc) */
        if ( typeof arg !== 'string' || !arg || typeof self.data.colors_textures[ arg ] !== 'string' )
            return errors.throw( 'Must provide a valid colour.' )

        return self.data.colors_textures[ arg ]

    case 'colors':
    case 'colors.terrain':
    /* An object of all terrain colour names and hexadecimal values */
        return JSON.parse( JSON.stringify( self.data.colors ) )

    case 'colors.textures':
    /* An object of all texture colour names and hexadecimal values */
        return JSON.parse( JSON.stringify( self.data.colors_textures ) )

    case 'data':
    /* The data structure from which the map can be redrawn */
        return JSON.parse( JSON.stringify( self.data ) )

    case 'zoom':
    /* The current zoom level of the map, between 0 and 4 */
        return self.getZoom()

    default:
        return errors.throw( 'Must provide a valid key.' )

    }
}