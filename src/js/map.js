import init from '@/api/init'
import destroy from '@/api/destroy'
import errors from '@/util/errors'
import get from '@/api/get'
import set from '@/api/set'
import action from '@/api/action'

console.info(
    `
    ——————————————————————————————————————
    Map editor provided by Atlas Fantasia
    © ${ new Date().getFullYear() }
    http://www.atlasfantasia.com
    ——————————————————————————————————————

`,
)

const app = function() {
    if ( !this || !( this instanceof AtlasFantasiaMap ) )
        errors.fatal( 'The AtlasFantasiaMap constructor must be called using `new`' )

    this.window = window

    this.initialised = false

    this.data = null

    this.action = 'none'

    this.public = {
        el: null,
    }

    /* define public properties as read-only */
    Object.defineProperty( this.public, 'init', {
        value: ( selector, config ) => init( this, selector, config ),
        writable: false,
    } )
    Object.defineProperty( this.public, 'get', {
        value: ( key, arg ) => get( this, key, arg ),
        writable: false,
    } )
    Object.defineProperty( this.public, 'set', {
        value: ( key, arg ) => set( this, key, arg ),
        writable: false,
    } )
    Object.defineProperty( this.public, 'action', {
        value: ( key, arg ) => action( this, key, arg ),
        writable: false,
    } )
    Object.defineProperty( this.public, 'destroy', {
        value: () => destroy( this ),
        writable: false,
    } )

    /* enable or disable debugging */
    // this.debug = true

    return this.public
}

window.AtlasFantasiaMap = app

export const map = app
