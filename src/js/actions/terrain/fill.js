import L from 'leaflet'
import polygon from '@/util/polygon'
import {
    mapRender,
} from '@/core/regions/map'

export default ( self, color ) => {
    Object.keys( self.data.terrain ).forEach( terrain => {
        self.data.terrain[ terrain ] = []
    } )
    self.data.terrain[ color ] = polygon( polygon( [
        [ 0, 0 ],
        [ 0, 1 ],
        [ 1, 1 ],
        [ 1, 0 ],
        [ 0, 0 ],
    ] ) )

    mapRender( self )
}