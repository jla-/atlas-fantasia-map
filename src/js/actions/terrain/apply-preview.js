import PolygonClipping from 'polygon-clipping'
import polygon from '@/util/polygon'
import {
    mapRender,
} from '@/core/regions/map'

let maxBounds,
    w, h, m, n, x, y, z, bounds

export const initApplyPreview = self => {
    maxBounds = polygon( [
        [ self.maxBounds[ 0 ][ 0 ], self.maxBounds[ 0 ][ 1 ] ],
        [ self.maxBounds[ 0 ][ 0 ], self.maxBounds[ 1 ][ 1 ] ],
        [ self.maxBounds[ 1 ][ 0 ], self.maxBounds[ 1 ][ 1 ] ],
        [ self.maxBounds[ 1 ][ 0 ], self.maxBounds[ 0 ][ 1 ] ],
        [ self.maxBounds[ 0 ][ 0 ], self.maxBounds[ 0 ][ 1 ] ],
    ] )
}

export default ( self, coords, color ) => {
    bounds = self.map.getBounds()
    m = bounds._southWest.lat
    n = bounds._southWest.lng
    w = Math.abs( bounds._northEast.lng - bounds._southWest.lng ) / self.preview.width
    h = Math.abs( bounds._northEast.lat - bounds._southWest.lat ) / self.preview.height

    for ( x = coords.length; x-- > 0; ) {
        for ( y = coords[ x ].length; y-- > 0; ) {
            for ( z = coords[ x ][ y ].length; z-- > 0; ) {
                coords[ x ][ y ][ z ][ 0 ] = w * coords[ x ][ y ][ z ][ 0 ] + n
                coords[ x ][ y ][ z ][ 1 ] = h * coords[ x ][ y ][ z ][ 1 ] + m
            }
        }
    }

    if ( !self.data.terrain[ color ] )
        self.data.terrain[ color ] = []

    self.data.terrain[ color ] = PolygonClipping.union( self.data.terrain[ color ], PolygonClipping.intersection( coords, maxBounds ) )

    Object.keys( self.data.terrain ).forEach( terrain => {
        if ( terrain !== color )
            self.data.terrain[ terrain ] = PolygonClipping.difference( self.data.terrain[ terrain ], self.data.terrain[ color ] )
    } )

    mapRender( self, 'terrain' )

}