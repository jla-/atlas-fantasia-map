import PolygonClipping from 'polygon-clipping'
import errors from '@/util/errors'
import polygon from '@/util/polygon'
import applyPreview from './apply-preview'
import {
    initApplyPreview,
} from './apply-preview'
import renderPreview from './render-preview'

let data = [],
    ctx = null,
    lastX = null,
    lastY = null,
    color = null,
    size = null

export const init = self => {
    ctx = self.preview.getContext( '2d' )
    initApplyPreview( self )
}

export const begin = ( self, d ) => {
    if (
        typeof d !== 'object' ||
        typeof d.color !== 'string' ||
        isNaN( +d.brush ) ||
        !d.color ||
        !d.brush
    )
        return errors.fatal( 'Must set a valid colour and size' )

    ctx.setLineDash( [ 2, 4 ] )
    ctx.lineWidth = 2
    ctx.lineCap = 'round'
    ctx.fillStyle = '#a39d89'
    ctx.strokeStyle = '#423716'
    color = d.color
    size = d.brush * 20
    reset( self )
    return self
}

export const preview = ( self, x, y ) => {
    if ( color && size ) {
        if (
            isNaN( lastX ) ||
            isNaN( lastY ) ||
            Math.sqrt( Math.pow( x - lastX, 2 ) + Math.pow( y - lastY, 2 ) ) > size / 4
        ) {
            let shape = [],
                hyp,
                pointX,
                pointY

            [
                0, 0.39269908169872414, 0.7853981633974483, 1.1780972450961724,
                1.5707963267948966, 1.9634954084936207, 2.356194490192345, 2.748893571891069,
                3.141592653589793, 3.534291735288517, 3.9269908169872414, 4.319689898685966,
                4.71238898038469, 5.105088062083414, 5.497787143782138, 5.8904862254808625,
            ].forEach( angle => {
                hyp = ( Math.random() - 0.5 ) * size / 5 + size / 2
                pointX = x + hyp * Math.cos( angle )
                pointY = y + hyp * Math.sin( angle )
                shape.push( [ pointX, pointY ] )
            } )
            shape = polygon( shape )
            data = PolygonClipping.union( data, shape )
            lastX = x
            lastY = y
            setTimeout( () => renderPreview( self, data, ctx ), 0 )
        }
    }
}

export const apply = self => {
    applyPreview( self, data, color )
    reset( self )
}

const reset = self => {
    data = []
    ctx.clearRect( 0, 0, self.preview.width, self.preview.height )
}
