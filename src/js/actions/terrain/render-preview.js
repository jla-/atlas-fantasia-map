export default ( self, data, ctx ) => {
    var lengthX = data.length,
        lengthY,
        lengthZ,
        x, y, z

    if ( lengthX ) {
        for ( x = 0; x < lengthX; x++ ) {

            lengthY = data[ x ].length
            ctx.beginPath()
            lines( data[ x ][ 0 ] )

            if ( lengthY > 1 ) {
                for ( y = 1; y < lengthY; y++ )
                    lines( data[ x ][ y ] )

            }

            ctx.save()
            ctx.clip( ( lengthY > 1 ? 'evenodd' : 'nonzero' ) )
            ctx.fill()
            ctx.stroke()
            ctx.restore()
        }
    }

    function lines( geometry ) {
        if ( geometry && Array.isArray( geometry ) ) {
            ctx.moveTo(
                Math.floor( geometry[ 0 ][ 0 ] ),
                Math.floor( geometry[ 0 ][ 1 ] ),
            )
            lengthZ = geometry.length
            for ( z = 0; z < lengthZ; z++ ) {
                ctx.lineTo(
                    Math.floor( geometry[ z ][ 0 ] ),
                    Math.floor( geometry[ z ][ 1 ] ),
                )
            }
            ctx.closePath()
        }
    }
}
