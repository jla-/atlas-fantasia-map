import brush from '@/core/regions/brush'
import {
    mapRender,
} from '@/core/regions/map'

let svg = null,
    done = false

export const init = self => {
    self.textEls = {}
}

export const begin = ( self, d ) => {
    svg = d.svg
    done = false
}

export const apply = self => {
    if ( done || !svg )
        return

    let id = `${ Date.now() }${ Math.random() }`,
        boundsText = svg.getBoundingClientRect(),
        boundsMap = self.map.getBounds(),
        wrapBounds = self.brushWrap.getBoundingClientRect(),
        m = boundsMap._southWest.lat,
        n = boundsMap._southWest.lng,
        w = Math.abs( boundsMap._northEast.lng - boundsMap._southWest.lng ) / self.preview.width,
        h = Math.abs( boundsMap._northEast.lat - boundsMap._southWest.lat ) / self.preview.height,
        x1 = w * ( boundsText.left - wrapBounds.left ) + n,
        x2 = w * ( boundsText.right - wrapBounds.left ) + n,
        y1 = h * ( boundsText.bottom - wrapBounds.top ) + m,
        y2 = h * ( boundsText.top - wrapBounds.top ) + m

    self.data.text[ id ] = {
        el: svg.outerHTML,
        bounds: [
            [ x2, y2 ],
            [ x1, y1 ],
        ],
    }
    self.textEls[ id ] = svg

    brush( self )
    svg = null
    done = true

    mapRender( self, 'text' )
}
