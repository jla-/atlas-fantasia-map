import errors from '@/util/errors'

export default ( self, arg ) => {

    if ( !self.fonts[ arg.font ] )
        return errors.throw( `The font "${ arg.font }" has not been loaded.` )

    let opentypePath = self.fonts[ arg.font ].getPath( arg.text, 0, 0, arg.size * 20 ),
        fontPath = opentypePath.toSVG( 3 ),
        svg = document.createElementNS( 'http://www.w3.org/2000/svg', 'svg' ),
        namespace = svg.namespaceURI,
        tmpParent = document.createElement( 'div' )

    svg.innerHTML = fontPath
    let path = svg.querySelector( 'path' )
    path.setAttribute( 'transform', `rotate(${ arg.rotation * 57.2957795131 })` )
    path.setAttribute( 'fill', arg.color )
    path.setAttribute( 'stroke', 'none' )

    tmpParent.setAttribute( 'style', 'position:fixed;top:0;left:0;z-index:-999;width:0;height:0;' )
    tmpParent.appendChild( svg )
    self.container.appendChild( tmpParent )

    let bounds = path.getBoundingClientRect(),
        w = Math.ceil( bounds.width * 1.2 ) // sometimes clips the end off so make it a bit wider
            .toString(),
        h = Math.ceil( bounds.height * 1.2 )
            .toString(),
        top = Math.ceil( Math.abs( bounds.top ) )
            .toString(),
        left = Math.ceil( Math.abs( bounds.left ) )
            .toString()

    self.container.removeChild( tmpParent )
    tmpParent = null

    svg.setAttribute( 'width', w )
    svg.setAttribute( 'height', h )
    svg.setAttribute( 'viewBox', `0 0 ${ w } ${ h }` )
    path.setAttribute( 'transform', `translate(${ left },${ top }) rotate(${ arg.rotation * 57.2957795131 })` )

    return svg
}
