export default ( type, canvas, done ) => {
    if ( type === 'canvas' )
        done( canvas )
    else {
        canvas.toBlob( function( blob ) {
            let img = document.createElement( 'img' ),
                url = URL.createObjectURL( blob )

            img.onload = () => done( img )
            img.src = url
        }, ( type === 'png' ? 'image/png' : 'image/jpeg' ) )
    }
}