const convert = ( svg, cb ) => {
    svg.removeAttribute( 'style' )
    let canvas = document.createElement( 'canvas' ),
        ctx = canvas.getContext( '2d' ),
        image = document.createElement( 'img' ),
        serializer = new XMLSerializer(),
        svgURL = serializer.serializeToString( svg )

    image.onload = function() {
        canvas.height = image.height
        canvas.width = image.width
        ctx.drawImage( image, 0, 0 )
        cb( canvas )
    }
    image.src = `data:image/svg+xml;charset=utf8,${ encodeURIComponent( svgURL ) }`
}

export default ( svg, ctx, done ) => {
    svg.style.position = 'absolute'
    const svgBounds = svg.getBoundingClientRect(),
        svgLeft = svgBounds.left,
        svgTop = svgBounds.top,
        svgWidth = svgBounds.width,
        svgHeight = svgBounds.height

    convert( svg, canvas => {
        ctx.drawImage( canvas, svgLeft, svgTop, svgWidth, svgHeight )
        done()
    } )
}