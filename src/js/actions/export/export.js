import output from './output'
import drawSvg from './svg'
import tiles from '@/core/tiles/tiles'

export default ( self, type, zoom, done, tick ) => {
    let sideCount = Math.pow( 2, zoom + 1 ),
        width = self.tileWidth * sideCount,
        height = self.tileHeight * sideCount,
        layers = [ 'terrain', 'textures', 'text' ],
        total = sideCount * sideCount * layers.length,
        counter = 0,
        inc = 1 / sideCount,
        x, y,
        canvas = document.createElement( 'canvas' ),
        ctx = canvas.getContext( '2d' ),
        fonts = {}

    canvas.width = width
    canvas.height = height

    layers.forEach( layer => {
    // BUG: inner loop uses callback, so on a small enough map or slow enough client layers may get drawn out of order. Need to await inner loops
        for ( x = 0; x < 1; x += inc ) {
            for ( y = 0; y < 1; y += inc ) {
                let _x = x,
                    _y = y,
                    bounds = {
                        _northEast: {
                            lat: y + inc,
                            lng: x + inc,
                        },
                        _southWest: {
                            lat: y,
                            lng: x,
                        },
                    };

                tiles( self, bounds, layer, ( _env, tile ) => {
                    if ( tile instanceof HTMLCanvasElement ) {
                        // If layer is canvas
                        draw( tile, _x, _y )

                    } else if ( tile instanceof HTMLElement ) {
                        // If layer is div with child svgs
                        let tmpCanvas = document.createElement( 'canvas' ),
                            tmpCtx = tmpCanvas.getContext( '2d' ),
                            tmpParent = document.createElement( 'div' ),
                            svgs = tile.querySelectorAll( 'svg' ),
                            c = 1

                        tmpCanvas.width = self.tileWidth
                        tmpCanvas.height = self.tileHeight

                        tile.setAttribute( 'style', `width:${ self.tileWidth }px;height:${ self.tileHeight }px;position:relative;` )
                        tmpParent.setAttribute( 'style', 'position:fixed;top:0;left:0;z-index:-999;width:0;height:0;overflow:hidden;opacity:0' )
                        tmpParent.appendChild( tile )
                        self.container.appendChild( tmpParent )

                        svgs.forEach( svg => {
                            drawSvg( svg, tmpCtx, () => {
                                if ( c++ >= svgs.length ) {
                                    self.container.removeChild( tmpParent )
                                    tmpParent = null
                                    draw( tmpCanvas, _x, _y )
                                }
                            } )
                        } )

                        if ( !svgs.length )
                            draw( tmpCanvas, _x, _y )
                    }

                } )
            }
        }
    } )

    function draw( layerCanvas, x, y ) {
        ctx.drawImage( layerCanvas, x * width, y * width, self.tileWidth, self.tileHeight )

        counter++
        if ( counter >= total )
            output( type, canvas, done )
        else {
            if ( typeof tick === 'function' )
                tick( counter / total )
        }
    }
}