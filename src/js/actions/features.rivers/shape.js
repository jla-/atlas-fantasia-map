import PolygonClipping from 'polygon-clipping'
import errors from '@/util/errors'
import polygon from '@/util/polygon'
import renderPreview from './render-preview'
import applyPreview from './apply-preview'

let data = [],
    ctx = null,
    lastX = null,
    lastY = null,
    thisX,
    thisY,
    size = null,
    tapering = null

export const init = self => {
    ctx = self.preview.getContext( '2d' )
}

export const begin = ( self, d ) => {
    if (
        typeof d !== 'object' ||
        isNaN( +d.size ) ||
        !d.size ||
        isNaN( +d.tapering ) ||
        !d.tapering
    )
        return errors.fatal( 'Must define a valid size and tapering' )

    size = d.size * 5
    tapering = d.tapering / 10
    ctx.lineWidth = size
    ctx.lineJoin = 'round'
    ctx.lineCap = 'round'
    ctx.strokeStyle = '#163142'
    reset( self )
    return self
}

export const preview = ( self, x, y ) => {
    if ( size ) {
        let hyp = Math.sqrt( Math.pow( x - lastX, 2 ) + Math.pow( y - lastY, 2 ) )
        if (
            isNaN( lastX ) ||
            isNaN( lastY ) ||
            ( hyp > 5 )
        ) {
            data.push( [ x, y ] )
            lastX = x
            lastY = y
            setTimeout( () => renderPreview( self, data, ctx, x, y ), 0 )
        } else
            setTimeout( () => renderPreview( self, data, ctx, x, y ), 0 )

        thisX = x
        thisY = y
    }
}

export const apply = self => {
    if ( !isNaN( thisX ) && !isNaN( thisY ) )
        data.push( [ thisX, thisY ] )

    applyPreview( self, data, size, tapering )
    reset( self )
}

const reset = self => {
    data = []
    ctx.clearRect( 0, 0, self.preview.width, self.preview.height )
}
