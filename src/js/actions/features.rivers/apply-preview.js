import PolygonClipping from 'polygon-clipping'
import polygon from '@/util/polygon'
import {
    mapRender,
} from '@/core/regions/map'

export default ( self, data, size, tapering ) => {
    // gonna square with you, idk how the hell this code works anymore.
    // I started from https://stackoverflow.com/a/46518285/1427563 and
    // things sort of took off from there. worked when i tested it and
    // that's good enough for now. If you need to debug this section
    // you might want to start from scratch, but i have scattered some
    // comments throughout that might help with the general logic.
    let length = data.length,
        polys = [],
        side1 = [],
        side2 = [],
        x, y, z,
        taper,
        bounds = self.map.getBounds(),
        m = bounds._southWest.lat,
        n = bounds._southWest.lng,
        w = Math.abs( bounds._northEast.lng - bounds._southWest.lng ) / self.preview.width,
        h = Math.abs( bounds._northEast.lat - bounds._southWest.lat ) / self.preview.height,
        scaledSize = Math.max( w, h ) * size / 2,
        x0, y0, x1, x2, y1, y2,
        px, py,
        len

    for ( x = length; x-- > 1; ) { // go along all points on the line
        y = x - 1
        z = x + 1

        x1 = w * data[ x ][ 0 ] + n // scale current point
        x2 = w * data[ y ][ 0 ] + n
        y1 = h * data[ x ][ 1 ] + m // scale next point
        y2 = h * data[ y ][ 1 ] + m
        x0 = z < length ? w * data[ z ][ 0 ] + n : x1 // scale previous point (if any)
        y0 = z < length ? h * data[ z ][ 1 ] + m : y1

        px = y0 - y2 // black magic from that SO answer to get perpendicular line between next and previous while intersecting at current
        py = x2 - x0
        taper = 1 - ( ( 1 - ( length - x ) / ( length ) ) * tapering )
        len = taper * scaledSize / Math.sqrt( px * px + py * py ) // with tapering

        px *= len
        py *= len

        side1.push( [ x1 + px, y1 + py ] ) // add to left and right sides
        side2.unshift( [ x1 - px, y1 - py ] )

        if ( x === 1 ) {
            side1.push( [ x2 + px, y2 + py ] )
            side2.unshift( [ x2 - px, y2 - py ] )
        }
    }

    polys = polygon( [ side1.concat( side2 ) ] ) // combine sides into a single polygon

    if ( !Array.isArray( self.data.terrain.water ) )
        self.data.terrain.water = []

    self.data.terrain.water = PolygonClipping.union( self.data.terrain.water, polys )

    Object.keys( self.data.terrain ).forEach( terrain => {
        if ( terrain !== 'water' )
        {self.data.terrain[ terrain ] = PolygonClipping.difference(
            self.data.terrain[ terrain ],
            self.data.terrain.water,
        )}
    } )

    mapRender( self, 'terrain' )
}