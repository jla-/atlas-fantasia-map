export default ( self, data, ctx, x, y ) => {
    ctx.clearRect( 0, 0, self.preview.width, self.preview.height )

    let l = data.length
    if ( l ) {
        ctx.beginPath()
        ctx.moveTo( data[ 0 ][ 0 ], data[ 0 ][ 1 ] )

        for ( let x = 1; x < l; x++ )
            ctx.lineTo( data[ x ][ 0 ], data[ x ][ 1 ] )

        ctx.lineTo( x, y )
        ctx.stroke()
    }
}