import PolygonClipping from 'polygon-clipping'
import polygon from '@/util/polygon'
import {
    mapRender,
} from '@/core/regions/map'

let maxBounds,
    w, h, m, n, x, y, z, bounds,
    terrainAll

export const initApplyPreview = self => {
    maxBounds = polygon( [
        [ self.maxBounds[ 0 ][ 0 ], self.maxBounds[ 0 ][ 1 ] ],
        [ self.maxBounds[ 0 ][ 0 ], self.maxBounds[ 1 ][ 1 ] ],
        [ self.maxBounds[ 1 ][ 0 ], self.maxBounds[ 1 ][ 1 ] ],
        [ self.maxBounds[ 1 ][ 0 ], self.maxBounds[ 0 ][ 1 ] ],
        [ self.maxBounds[ 0 ][ 0 ], self.maxBounds[ 0 ][ 1 ] ],
    ] )
}

export default ( self, coords, texture, terrain ) => {
    bounds = self.map.getBounds()
    m = bounds._southWest.lat
    n = bounds._southWest.lng
    w = Math.abs( bounds._northEast.lng - bounds._southWest.lng ) / self.preview.width
    h = Math.abs( bounds._northEast.lat - bounds._southWest.lat ) / self.preview.height

    for ( x = coords.length; x-- > 0; ) {
        for ( y = coords[ x ].length; y-- > 0; ) {
            for ( z = coords[ x ][ y ].length; z-- > 0; ) {
                coords[ x ][ y ][ z ][ 0 ] = w * coords[ x ][ y ][ z ][ 0 ] + n
                coords[ x ][ y ][ z ][ 1 ] = h * coords[ x ][ y ][ z ][ 1 ] + m
            }
        }
    }

    if ( texture === 'erase' ) {
        Object.keys( self.data.textures ).forEach( t => {
            self.data.textures[ t ] = PolygonClipping.difference( self.data.textures[ t ], coords )
        } )
    } else {
        terrainAll = []
        terrain.forEach( t => {
            if ( self.data.terrain[ t ] )
                terrainAll = PolygonClipping.union( terrainAll, self.data.terrain[ t ] )
        } )

        if ( !self.data.textures[ texture ] )
            self.data.textures[ texture ] = []

        self.data.textures[ texture ] = PolygonClipping.union( self.data.textures[ texture ], PolygonClipping.intersection( coords, maxBounds, terrainAll ) )
    }
    mapRender( self, 'textures' )
}
