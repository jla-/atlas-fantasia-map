import PolygonClipping from 'polygon-clipping'
import errors from '@/util/errors'
import polygon from '@/util/polygon'
import renderPreview from '@/actions/terrain/render-preview'
import applyPreview from './apply-preview'
import {
    initApplyPreview,
} from './apply-preview'

let data = [],
    ctx = null,
    lastX = null,
    lastY = null,
    texture = null,
    terrain = [],
    size = null

export const init = self => {
    ctx = self.preview.getContext( '2d' )
    initApplyPreview( self )
}

export const begin = ( self, d ) => {
    if (
        typeof d !== 'object' ||
        typeof d.texture !== 'string' ||
        isNaN( +d.brush ) ||
        !d.texture ||
        !d.brush
    )
        return errors.fatal( 'Must define a valid texture and size' )

    ctx.setLineDash( [ 2, 4 ] )
    ctx.lineWidth = 2
    ctx.lineCap = 'round'
    ctx.fillStyle = '#8999a3'
    ctx.strokeStyle = '#163142'
    texture = d.texture
    terrain = d.terrain || []
    size = d.brush * 20
    reset( self )
    return self
}

export const preview = ( self, x, y ) => {
    if ( texture && size ) {
        if (
            isNaN( lastX ) ||
            isNaN( lastY ) ||
            Math.sqrt( Math.pow( x - lastX, 2 ) + Math.pow( y - lastY, 2 ) ) > size / 12
        ) {
            let shape = [],
                hyp,
                point_x,
                pointY

            [
                0, 0.39269908169872414, 0.7853981633974483, 1.1780972450961724,
                1.5707963267948966, 1.9634954084936207, 2.356194490192345, 2.748893571891069,
                3.141592653589793, 3.534291735288517, 3.9269908169872414, 4.319689898685966,
                4.71238898038469, 5.105088062083414, 5.497787143782138, 5.8904862254808625,
            ].forEach( angle => {
                hyp = size / 2
                point_x = x + hyp * Math.cos( angle )
                pointY = y + hyp * Math.sin( angle )
                shape.push( [ point_x, pointY ] )
            } )
            shape = polygon( shape )
            data = PolygonClipping.union( data, shape )
            lastX = x
            lastY = y
            setTimeout( () => renderPreview( self, data, ctx ), 0 )
        }
    }
}

export const apply = self => {
    applyPreview( self, data, texture, terrain )
    reset( self )
}

const reset = self => {
    data = []
    ctx.clearRect( 0, 0, self.preview.width, self.preview.height )
}
