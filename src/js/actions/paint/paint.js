import {
    begin as beginTerrain,
    preview as previewTerrain,
    apply as applyTerrain,
} from '@/actions/terrain/paint'
import {
    begin as beginTextures,
    preview as previewTextures,
    apply as applyTextures,
} from '@/actions/textures/paint'
import {
    begin as beginText,
    apply as applyText,
} from '@/actions/text/paint'
import {
    begin as beginFeaturesRivers,
    preview as previewFeaturesRivers,
    apply as applyFeaturesRivers,
} from '@/actions/features.rivers/shape'

let wrapBounds, x, y

export const beginPaint = ( self, data ) => {
    switch ( self.action ) {

    case 'terrain.paint':
        beginTerrain( self, data )
        break

    case 'textures.paint':
        beginTextures( self, data )
        break

    case 'features.shape.rivers':
        beginFeaturesRivers( self, data )
        break

    case 'text':
        beginText( self, data )
        break

    }
}

export const previewPaint = ( self, fixedX, fixedY ) => {
    wrapBounds = self.brushWrap.getBoundingClientRect()
    x = fixedX - wrapBounds.left
    y = fixedY - wrapBounds.top

    switch ( self.action ) {

    case 'terrain.paint':
        previewTerrain( self, x, y )
        break

    case 'textures.paint':
        previewTextures( self, x, y )
        break

    case 'features.shape.rivers':
        previewFeaturesRivers( self, x, y )
        break

    }

}

export const applyPaint = self => {
    switch ( self.action ) {

    case 'terrain.paint':
        applyTerrain( self )
        break

    case 'textures.paint':
        applyTextures( self )
        break

    case 'features.shape.rivers':
        applyFeaturesRivers( self )
        break

    case 'text':
        applyText( self )
        break

    }
}
