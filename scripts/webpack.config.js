const config = require( './config' ),
    TerserPlugin = require( 'terser-webpack-plugin' )

module.exports = {
    entry: {
        'index': `${ config.path.js }/map.js`
    },
    output: {
        path: config.path.dist,
        filename: '[name].js'
    },
    module: {
        rules: [ {
            test: /\.(js)$/,
            exclude: /node_modules/,
            use: 'babel-loader'
        } ]
    },
    optimization: {
        minimizer: [
            new TerserPlugin( {
                extractComments: false,
                terserOptions: {
                    output: {
                        comments: false,
                    },
                },
                parallel: true,
            } ),
        ],
    },
    resolve: {
        alias: {
            '@': config.path.js
        },
        extensions: [ '.js', '.json' ]
    },
    mode: 'production'
}