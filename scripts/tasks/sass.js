const autoprefixer = require( 'gulp-autoprefixer' ),
    config = require( '../config' ),
    gulp = require( 'gulp' ),
    sass = require( 'gulp-sass' ),
    rename = require( 'gulp-rename' )

module.exports = () => {
    return gulp.src( [ `${ config.path.sass }/map.scss` ] )
        .pipe( sass( {
            outputStyle: 'compressed'
        } ) )
        .pipe( autoprefixer() )
        .pipe( rename( path => {
            if ( path.basename === 'map' )
                path.basename = 'style'
        } ) )
        .pipe( gulp.dest( config.path.dist ) )
}