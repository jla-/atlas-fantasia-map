const gulp = require( 'gulp' )

module.exports = async () => {
    return await gulp.series( 'init', 'js', 'sass' )()
}