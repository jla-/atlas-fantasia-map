const config = require( '../config' ),
    eslint = require( 'gulp-eslint' ),
    gulp = require( 'gulp' ),
    gulpIf = require( 'gulp-if' ),
    eslintRules = require( '../.eslintrc' ),
    stylelint = require( 'gulp-stylelint' ),
    stylelintRules = require( '../.stylelintrc' )

const isFixed = file => {
    return file.eslint != null && file.eslint.fixed;
}

module.exports = async () => {
    await gulp.src( [ `${ config.path.sass }/**/*.scss` ] )
        .pipe( stylelint( {
            fix: true,
            config: stylelintRules,
            reporters: [ {
                formatter: 'string',
                console: true
            } ]
        } ) )
        .pipe( gulp.dest( config.path.sass ) )

    return gulp.src( [ `${ config.path.js }/**/*.js` ] )
        .pipe( eslint( eslintRules ) )
        .pipe( eslint.format() )
        .pipe( eslint.failAfterError() )
        .pipe( gulpIf( isFixed, gulp.dest( config.path.js ) ) )
}