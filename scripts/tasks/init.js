const fs = require( 'fs' ),
    gulp = require( 'gulp' ),
    config = require( '../config' )

module.exports = () => {
    return new Promise( ( resolve, reject ) => {
        if ( !fs.existsSync( config.path.dist ) )
            fs.mkdirSync( config.path.dist )
        resolve()
    } )
}