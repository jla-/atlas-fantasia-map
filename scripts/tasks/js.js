const webpack = require( 'webpack' ),
    webpackConfig = require( '../webpack.config.js' )

module.exports = () => {
    return new Promise( ( resolve, reject ) => {
        webpack( webpackConfig, ( err, stats ) => {
            if ( err ) {
                return reject( err )
            }
            if ( stats.hasErrors() ) {
                return reject( new Error( stats.compilation.errors.join( '\n' ) ) )
            }
            resolve()
        } )
    } )
}