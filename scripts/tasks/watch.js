const config = require( '../config' ),
    gulp = require( 'gulp' )

module.exports = () => {
    gulp.series( 'init', 'js', 'sass' )()
    gulp.watch( `${ config.path.js }/**/*.js`, gulp.series( 'js' ) )
    gulp.watch( `${ config.path.sass }/**/*.scss`, gulp.series( 'sass' ) )
}