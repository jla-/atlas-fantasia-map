const path = require( 'path' )

module.exports = {
    path: {
        dist: path.resolve( __dirname, '../dist' ),
        src: path.resolve( __dirname, '../src' ),
        js: path.resolve( __dirname, '../src/js' ),
        sass: path.resolve( __dirname, '../src/sass' ),
    }
}