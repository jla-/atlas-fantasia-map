# Contributing to the Atlas Fantasia Map project

Contributions to the Atlas Fantasia map project are very welcome. This file should help get you started. For more information please email `james@atlasfantasia.com` or open an issue on the gitlab.

## Installation for development

The Atlas Fantasia map project is written in JavaScript and Sass, and built with Gulp and Webpack. To install the development tools you will need to have Node/NPM installed, and the Gulp module installed globally. To install Gulp globally run

```bash
npm install -g gulp
```

Once this is done, navigate to the project's root directory and install the local development environment with

```bash
npm install
```

If you prefer using Yarn then replace the commands as necessary.

To build the project once run

```bash
gulp build
```

To build the project while watching for changes to and `.js` and `.scss` files run

```bash
gulp watch
```

By default WebPack will compile to a production ready build. If you want a development build with source maps then you can update the `webpack.config.js` file but please do not commit it.

#### Interface for development

The Atlas Fantasia map does not come with any sort of graphical editing interface. It is a library and those who utilise it are expected to construct their own GUI, interacting with the library via its powerful API. However understandably this makes development difficult for those who don't already have access to a working interface. In the future an interface built with Vue will be bundled. For now, if you wish to develop for the Atlas Fantasia map library, email james@atlasfantasia.com and you will be emailed an interface that should get you started.

## Coding Style

Configuration files for eslint and csscomb may be found in `/.eslintrc.json` and `/.csscomb.json`. It is recommended to set your IDE to lint using these configurations. The automated Codacy checker may raise issues if styles aren't followed consistently.

The most important style rules are summed up below:

-   Use four spaces for JavaScript indenting and two spaces for SCSS indenting.
-   Use single quotes for JavaScript strings and double quotes for SCSS strings.
-   Use Egyptian brackets for code blocks preceded by a key word or selector.
-   Use camelCase for variables. This rule hasn't been strictly followed in the past; if you find variables using snake_case feel free to submit a pull request or open an issue.

#### Spelling

All text that is visible through the rendered interface, and all documentation text in vernacular English must be spelt with British spelling. All keywords, variable names, and other text in code must be spelt with American spelling. Eg an object property might be `object.color` with documentation describing it as `object.color contains the colour value`.

## The data object
