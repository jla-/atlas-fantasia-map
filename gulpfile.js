/**
 * All Gulp tasks are in the /scripts/tasks directory. To add a new task create
 * a new js file with the name of the task and module.exports set to the task
 * function.
 */

const fs = require( 'fs' ),
    gulp = require( 'gulp' ),
    path = require( 'path' )

const dir = fs.opendirSync( './scripts/tasks' )
let file
while ( ( file = dir.readSync() ) !== null ) {
    const name = path.basename( file.name, '.js' ),
        task = require( `${ dir.path }/${ name }` )
    gulp.task( name, task )
}
dir.closeSync()